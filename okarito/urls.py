from django.conf.urls import patterns, include, url
from django.contrib import admin
from problemas.views import AjaxView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'okarito.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^ajax/$', AjaxView.as_view()),
    url(r'^$', include('problemas.urls'))
)

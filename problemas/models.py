# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class Tag(models.Model):
    tag = models.CharField(max_length=64, unique=True)
    tipo = models.CharField(max_length=32, blank=True)

    def __str__(self):
        return self.tag


@python_2_unicode_compatible
class Problema(models.Model):
    enunciado = models.TextField()
    anexo = models.CharField(max_length=128, blank=True)
    solucion = models.TextField(blank=True)
    desarrollo = models.TextField(blank=True)
    tags = models.ManyToManyField(Tag)
    comentarios = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.enunciado


class Figura(models.Model):
    tipo = models.CharField(max_length=5)
    imagen = models.ImageField()
    enunciado = models.ForeignKey(Problema)

    def __str__(self):
        return self.imagen.url

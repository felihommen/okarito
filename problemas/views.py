# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import TemplateView
from django.core import serializers
from django.http import HttpResponse
import json
from .models import Tag, Problema


class HomeView(TemplateView):
    template_name = 'problemas/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['etiquetas'] = Tag.objects.all()
        return context


class AjaxView(TemplateView):

    def get(self, request, *args, **kwargs):
        filtro = request.GET['_filtro_']
        pretiquetas = [etiqueta for etiqueta in request.GET.keys()]
        pretiquetas.remove('_filtro_')
        if not pretiquetas:
            etiquetas = Tag.objects.all()
        else:
            problemas = [problema.pk for problema in Problema.objects.filter(tags__tag__in = pretiquetas).distinct()]
            etiquetas = Tag.objects.filter(problema__pk__in=problemas).distinct()
        etiquetas = [etiqueta.tag for etiqueta in etiquetas.filter(tag__icontains=filtro) if etiqueta.tag not in pretiquetas]
        #data = serializers.serialize('json', etiquetas)
        data = json.dumps({'sel': pretiquetas, 'desel': etiquetas})
        return HttpResponse(data, content_type="application/json")

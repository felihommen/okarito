from django.conf.urls import patterns, include, url
from views import HomeView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'okarito.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', HomeView.as_view()),
)

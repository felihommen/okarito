# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from problemas.models import Problema, Tag, Figura

class ProblemaAdmin(admin.ModelAdmin):
    list_display = ('html_enunciado', )
    filter_horizontal = ('tags',)

    def html_enunciado(self, problema):
        return problema.enunciado

    html_enunciado.allow_tags = True


class TagAdmin(admin.ModelAdmin):
    list_display = ('tag',)

admin.site.register(Tag, TagAdmin)
admin.site.register(Figura)
admin.site.register(Problema, ProblemaAdmin)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Figura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=5)),
                ('imagen', models.ImageField(upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Problema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enunciado', models.TextField()),
                ('anexo', models.CharField(max_length=128, blank=True)),
                ('solucion', models.TextField(blank=True)),
                ('desarrollo', models.TextField(blank=True)),
                ('comentarios', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(unique=True, max_length=64)),
                ('tipo', models.CharField(max_length=32, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='problema',
            name='tags',
            field=models.ManyToManyField(to='problemas.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='figura',
            name='enunciado',
            field=models.ForeignKey(to='problemas.Problema'),
            preserve_default=True,
        ),
    ]
